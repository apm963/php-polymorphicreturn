# PHP PolymorphicReturn POC

This is a proof-of-concept for polymorphic return types (Generics) in PHP >= 5.3. This implementation differs a bit from traditional generics, as the requested type does not have to be passed in as a parameter. Also, it is not inferred (although a narrow implementation is possible.

## Usage example

```php
$obj = new XMLExample();
$node_ID = 'node1';

var_dump( $obj->get($node_ID) );          // Object XMLNode
var_dump( $obj->get{string}($node_ID) );  // "Hello world"; <- contents
var_dump( $obj->get{boolean}($node_ID) ); // true           <- node exists

// Custom directives
var_dump( $obj->get{'XML'}($node_ID) );  // "<node1>Hello world</node1>"
```
