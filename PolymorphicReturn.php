<?php

define('main', 'main');
define('boolean', 'boolean');
define('string', 'string');
define('integer', 'integer');
// array and object have reserved keywords so things aren't as easy with them
define('arr', 'array');
define('obj', 'object');

class PolymorphicReturnBase {
	
	protected $method_type_separator = '__';
	
	public function __get($prop) {
		
		$all_types    = array(
			'main'    => '',
			'boolean' => 'boolean',
			'bool'    => 'boolean',
			'string'  => 'string',
			'str'     => 'str',
			'integer' => 'integer',
			'int'     => 'integer',
			'float'   => 'float',
			'double'  => 'double',
			'array'   => 'array',
			'object'  => 'object',
		);
		
		$polymorphic_implementations = array();
		
		$_this = &$this; // For the function
		
		foreach ($all_types as $type => $full_type) {
			
			$func_name = $prop.($full_type !== '' ? "{$this->method_type_separator}{$full_type}" : '');
			
			if (method_exists($this, $func_name)) {
				$polymorphic_implementations[$type] = (function () use (&$_this, $func_name, $full_type) {
					
					// Call typed method
					$ret = call_user_func_array( array($_this, $func_name), func_get_args() );
					
					// Validate return type
					$_this->validate_return($func_name, $ret, $full_type);
					
					return $ret;
				});
			}
			
		}
		
		//echo '<pre>'.print_r($polymorphic_implementations, true).'</pre>';
		
		// This return value can be further refined to be an object that implements ArrayAccess. It can then have custom error handling and the potential for more.
		return $polymorphic_implementations;
	}
	
	public function validate_return($func_name, $ret, $expected) {
		
		if ($expected === '') {
			// Nothing expected
			return;
		}
		
		$type = gettype($ret);
		
		if (strtolower($type) !== strtolower($expected)) {
			trigger_error('Expected return data type to be ['.$expected.'], got ['.$type.']', E_USER_NOTICE);
		}
		
		return;
	}
	
}

class PolymorphicReturnBase· extends PolymorphicReturnBase {
	// The sole purpose of this class is make the special delimiter character easily accessible in the class name
	// so that when it is extended, consumers won't have to hunt for the delimiter in an ASCII chart.
	protected $method_type_separator = '·';
}

class PolymorphicReturn extends PolymorphicReturnBase· {
	// This could potentially be bolstered with traits or dependency injection
	// This could further be improved by adding another delimiter in the function name for parameter definition
	
	public function find($haystack, $needle) {
		return strpos($haystack, $needle);
	}
	
	// These methods have to be public due to the scope of the callback
	// Method name doesn't need to have a non-keyboard character. Could be something like a double underscore.
	public function find·boolean($haystack, $needle) {
		#return (call_user_func_array( array($this, 'find'), func_get_args() ) !== false); // This can be used without requiring param definition
		return ($this->find($haystack, $needle) !== false);
	}
	
	public function find·string($haystack, $needle) {
		// Not sure what good this method would do without an additional parameter
		$pos = $this->find($haystack, $needle);
		return (
			$pos !== false
			? $needle
			: ''
		);
	}
	
	public function find·array($haystack, $needle) {
		$find_res = $this->find($haystack, $needle);
		return array(
			'haystack' => $haystack,
			'needle' => $needle,
			'found' => ($find_res !== false),
			'location' => ($find_res !== false ? $find_res : null),
		);
	}
	
	public function find·object($haystack, $needle) {
		return json_decode( json_encode( $this->find·array($haystack, $needle) ) );
	}
	
	// All of these separators could potentially work (see chart under extended ASCII codes here: http://www.ascii-code.com/ )
	// These are kind of nice because syntax highlighting in Notepad++ should highlight the keyword (boolean, integer, array, etc.)
	public function finarray($haystack, $needle) { }
	public function findƒarray($haystack, $needle) { }
	public function find…array($haystack, $needle) { }
	public function findˆarray($haystack, $needle) { }
	public function find‹array($haystack, $needle) { }
	public function find›array($haystack, $needle) { }
	public function find•array($haystack, $needle) { }
	public function find–array($haystack, $needle) { }
	public function find—array($haystack, $needle) { }
	public function find˜array($haystack, $needle) { }
	public function find¤array($haystack, $needle) { }
	public function find¦array($haystack, $needle) { }
	public function find«array($haystack, $needle) { }
	public function find»array($haystack, $needle) { }
	public function find¬array($haystack, $needle) { }
	public function find°array($haystack, $needle) { }
	public function find¶array($haystack, $needle) { }
	//public function find·array($haystack, $needle) { }
	public function find×array($haystack, $needle) { }
	public function findÞarray($haystack, $needle) { }
	public function findþarray($haystack, $needle) { }
	public function findøarray($haystack, $needle) { }
	
}

$T = new PolymorphicReturn();

$haystack = 'the quick brown fox';
$needle = 'quick';

var_dump( $T->find($haystack, $needle) ); echo '<br />';
var_dump( $T->find{main}($haystack, $needle) ); echo '<br />';
var_dump( $T->find{boolean}($haystack, $needle) ); echo '<br />';
var_dump( $T->find{string}($haystack, $needle) ); echo '<br />';
var_dump( $T->find{arr}($haystack, $needle) ); echo '<br />';
var_dump( $T->find{obj}($haystack, $needle) ); echo '<br />';
var_dump( $T->find{obj}($haystack, $needle)->location ); echo '<br />';

// Square bracket notation also works. It, however, gives away the magic that the return value is actually an array.
var_dump( $T->find[main]($haystack, $needle) ); echo '<br />';

// Simplified, here is what is going on here. $T->find is a public variable and is visible from the current scope so it is called.
// When the brackets are introduced after the method name, 'find' is treated as a property instead of a method because it is looking
// for an array. This calls the magic method __get() and an array is generated of supported types as key and lambda as value. The
// lambda takes any number of arguments and passes them to the method "{$method}{$delimiter}{$type}" using the object scope. That
// method then chooses to return whatever it is implemented to return. A notice will be generated if the return data type is different
// than the specified data type.


